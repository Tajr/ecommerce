# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Cart
from django.shortcuts import render, redirect
from products.models import Product,MainCategorie,SubCategorie,Categorie

# Create your views here.


def querysets():
	mainCats = MainCategorie.objects.all()
	subCats = SubCategorie.objects.all()
	categories = Categorie.objects.all()

	context = {
		'mainCats':mainCats, 
		'subCats':subCats,
		'categories':categories
		}

	return context


def cartView(request):
	tmp = 'cart.html'
	cart_obj, new_obj  = Cart.objects.new_or_get(request)
	mainCats = MainCategorie.objects.all()
	subCats = SubCategorie.objects.all()
	categories = Categorie.objects.all()
	
	context = {
			'cart':cart_obj,
			'mainCats':mainCats, 
			'subCats':subCats,
			'categories':categories
		}	
		
	return render(request,tmp,context)


def cart_update(request):
	product_id = request.POST.get("product_id")
	if product_id is not None:
		try:
			product_obj = Product.objects.get(id=product_id)
		except Product.DoesNotExist:
			print("Product is not in the stock")
			return redirect("/cart/")
		cart_obj, new_obj  = Cart.objects.new_or_get(request)
		if product_obj in cart_obj.products.all():
			cart_obj.products.remove(product_obj)
		else:
			cart_obj.products.add(product_obj)

	return redirect("/cart/")

