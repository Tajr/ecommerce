# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView, DetailView
from django.shortcuts import render
from .models import Product,MainCategorie,SubCategorie,Categorie
from cart.models import Cart
# Create your views here.

def querysets():
	mainCats = MainCategorie.objects.all()
	subCats = SubCategorie.objects.all()
	categories = Categorie.objects.all()

	context = {
		'mainCats':mainCats, 
		'subCats':subCats,
		'categories':categories
		}

	return context

def index(request):
	tmp = 'index.html'
	context = querysets()

	return render(request,tmp,context)

def department(request):
	tmp = 'department.html'

	context = querysets()


	return render(request,tmp,context)

def category(request, pk):
	tmp = 'category-full.html'
	categoriez = Categorie.objects.get(id=pk)
	products = Product.objects.all().filter(category=categoriez)
	mainCats = MainCategorie.objects.all()
	subCats = SubCategorie.objects.all()
	categories = Categorie.objects.all()
	cart_obj, new_obj = Cart.objects.new_or_get(request)

	categorySelected = pk

	context = {
		'mainCats':mainCats, 
		'subCats':subCats,
		'categories':categories,
		'products':products,
		'cart': cart_obj,
		'categorySelected':int(categorySelected)
		}
	return render(request,tmp,context)


class ProductDetailView(DetailView):
	model 					= 			Product
	context_object_name		=			'product'
	template_name 			=			'detail.html'

	def get_context_data(self, **kwargs):
		context = super(ProductDetailView, self).get_context_data(**kwargs)
		cart_obj, new_obj  = Cart.objects.new_or_get(self.request)

		context.update({
			'cart': cart_obj,
			'mainCats' : MainCategorie.objects.all(),
			'subCats' : SubCategorie.objects.all(),
			'categories' : Categorie.objects.all(),
			})
		
		return context