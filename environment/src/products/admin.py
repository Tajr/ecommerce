# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Product,MainCategorie,SubCategorie,Categorie


# admin.site.register(Product)
# admin.site.register(MainCategorie)
# admin.site.register(SubCategorie)
# admin.site.register(Categorie)

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
	list_display 	=	('id', 'title', 'description', 'price', 'category','Platform','in_stock','list_price','price','Savings')

@admin.register(MainCategorie)
class MainCategorieAdmin(admin.ModelAdmin):
	list_display 	=	('id', 'main_cat_name')

@admin.register(SubCategorie)
class SubCategorieAdmin(admin.ModelAdmin):
	list_display 	=	('id', 'sub_cat_name','main_category')

@admin.register(Categorie)
class CategorieAdmin(admin.ModelAdmin):
	list_display 	=	('id', 'cat_name','sub_category')