# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class MainCategorie(models.Model):
	main_cat_name	= models.CharField(max_length=120)
	cat_image		= models.ImageField(null=True)


	class Meta:
		ordering=['id']
			
	def __str__(self):
		return self.main_cat_name

class SubCategorie(models.Model):
	sub_cat_name	= models.CharField(max_length=120)
	main_category	= models.ForeignKey('MainCategorie',  on_delete=models.CASCADE)
	
	def __str__(self):
		return self.sub_cat_name

	class Meta:
		ordering=['id']
		
class Categorie(models.Model):
	cat_name 		= models.CharField(max_length=120)
	sub_category	= models.ForeignKey('SubCategorie',  on_delete=models.CASCADE)

	class Meta:
		ordering=['id']
		
	def __str__(self):
		return self.cat_name

class Product(models.Model):
	title			= models.CharField(max_length=120)
	Platform		= models.CharField(max_length=120,null=True)
	in_stock		= models.BooleanField(default=True)
	description		= models.TextField()
	list_price		= models.DecimalField(decimal_places=2,max_digits=10,default=0.00)
	price			= models.DecimalField(decimal_places=2,max_digits=10,default=0.00)
	Savings			= models.DecimalField(decimal_places=2,max_digits=10,default=0.00)
	category 		= models.ForeignKey('Categorie', on_delete=models.CASCADE,null=True)
	image			= models.ImageField(null=True)

	class Meta:
		ordering=['id']

	def __str__(self):
		return self.title
